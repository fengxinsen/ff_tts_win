//
//  Generated file. Do not edit.
//

// clang-format off

#include "generated_plugin_registrant.h"

#include <ff_tts_win/ff_tts_win_plugin_c_api.h>

void RegisterPlugins(flutter::PluginRegistry* registry) {
  FfTtsWinPluginCApiRegisterWithRegistrar(
      registry->GetRegistrarForPlugin("FfTtsWinPluginCApi"));
}
