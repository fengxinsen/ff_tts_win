import 'ff_tts_win_platform_interface.dart';

class FFTTSWin {
  Future<String?> getPlatformVersion() {
    return FfTtsWinPlatform.instance.getPlatformVersion();
  }

  static Future<bool?> init() {
    return FfTtsWinPlatform.instance.init();
  }

  static Future<bool?> setRate(int rate) {
    return FfTtsWinPlatform.instance.setRate(rate);
  }

  static Future<bool?> speak(String text) {
    return FfTtsWinPlatform.instance.speak(text);
  }

  static Future<bool?> dispose() {
    return FfTtsWinPlatform.instance.dispose();
  }
}
