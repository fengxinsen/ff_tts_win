import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'ff_tts_win_platform_interface.dart';

/// An implementation of [FfTtsWinPlatform] that uses method channels.
class MethodChannelFfTtsWin extends FfTtsWinPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('ff_tts_win');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<bool?> init() async {
    return await methodChannel.invokeMethod('init');
  }

  @override
  Future<bool?> setRate(int rate) async {
    return await methodChannel
        .invokeMethod('rate', <String, Object>{"rate": rate});
  }

  @override
  Future<bool?> speak(String text) async {
    return await methodChannel
        .invokeMethod('speak', <String, Object>{"text": text});
  }

  @override
  Future<bool?> dispose() async {
    return await methodChannel.invokeMethod('dispose');
  }
}
