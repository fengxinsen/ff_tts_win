import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'ff_tts_win_method_channel.dart';

abstract class FfTtsWinPlatform extends PlatformInterface {
  /// Constructs a FfTtsWinPlatform.
  FfTtsWinPlatform() : super(token: _token);

  static final Object _token = Object();

  static FfTtsWinPlatform _instance = MethodChannelFfTtsWin();

  /// The default instance of [FfTtsWinPlatform] to use.
  ///
  /// Defaults to [MethodChannelFfTtsWin].
  static FfTtsWinPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FfTtsWinPlatform] when
  /// they register themselves.
  static set instance(FfTtsWinPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<bool?> init() {
    throw UnimplementedError('init() has not been implemented.');
  }

  Future<bool?> setRate(int rate) {
    throw UnimplementedError('setRate() has not been implemented.');
  }

  Future<bool?> speak(String text) {
    throw UnimplementedError('speak() has not been implemented.');
  }

  Future<bool?> dispose() {
    throw UnimplementedError('dispose() has not been implemented.');
  }
}
