import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:ff_tts_win/ff_tts_win_method_channel.dart';

void main() {
  MethodChannelFfTtsWin platform = MethodChannelFfTtsWin();
  const MethodChannel channel = MethodChannel('ff_tts_win');

  TestWidgetsFlutterBinding.ensureInitialized();

  setUp(() {
    channel.setMockMethodCallHandler((MethodCall methodCall) async {
      return '42';
    });
  });

  tearDown(() {
    channel.setMockMethodCallHandler(null);
  });

  test('getPlatformVersion', () async {
    expect(await platform.getPlatformVersion(), '42');
  });
}
