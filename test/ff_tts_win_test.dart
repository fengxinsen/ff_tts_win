import 'package:flutter_test/flutter_test.dart';
import 'package:ff_tts_win/ff_tts_win.dart';
import 'package:ff_tts_win/ff_tts_win_platform_interface.dart';
import 'package:ff_tts_win/ff_tts_win_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFfTtsWinPlatform
    with MockPlatformInterfaceMixin
    implements FfTtsWinPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<bool?> dispose() => Future.value(true);

  @override
  Future<bool?> init() => Future.value(true);

  @override
  Future<bool?> setRate(int rate) => Future.value(true);

  @override
  Future<bool?> speak(String text) => Future.value(true);
}

void main() {
  final FfTtsWinPlatform initialPlatform = FfTtsWinPlatform.instance;

  test('$MethodChannelFfTtsWin is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFfTtsWin>());
  });

  test('getPlatformVersion', () async {
    FFTTSWin ffTtsWinPlugin = FFTTSWin();
    MockFfTtsWinPlatform fakePlatform = MockFfTtsWinPlatform();
    FfTtsWinPlatform.instance = fakePlatform;

    expect(await ffTtsWinPlugin.getPlatformVersion(), '42');
  });
}
