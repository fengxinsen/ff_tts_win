#ifndef FLUTTER_PLUGIN_FF_TTS_WIN_PLUGIN_H_
#define FLUTTER_PLUGIN_FF_TTS_WIN_PLUGIN_H_

#include <flutter/method_channel.h>
#include <flutter/plugin_registrar_windows.h>

#include <memory>

#include "tts.h"

namespace ff_tts_win {

	class FfTtsWinPlugin : public flutter::Plugin {
	public:
		static void RegisterWithRegistrar(flutter::PluginRegistrarWindows* registrar);

		FfTtsWinPlugin();

		virtual ~FfTtsWinPlugin();

		// Disallow copy and assign.
		FfTtsWinPlugin(const FfTtsWinPlugin&) = delete;
		FfTtsWinPlugin& operator=(const FfTtsWinPlugin&) = delete;

	private:
		// Called when a method is called on this plugin's channel from Dart.
		void HandleMethodCall(
			const flutter::MethodCall<flutter::EncodableValue>& method_call,
			std::unique_ptr<flutter::MethodResult<flutter::EncodableValue>> result);

		TTS tts = TTS();
	};

}  // namespace ff_tts_win

#endif  // FLUTTER_PLUGIN_FF_TTS_WIN_PLUGIN_H_
