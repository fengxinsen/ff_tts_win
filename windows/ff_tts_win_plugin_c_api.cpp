#include "include/ff_tts_win/ff_tts_win_plugin_c_api.h"

#include <flutter/plugin_registrar_windows.h>

#include "ff_tts_win_plugin.h"

void FfTtsWinPluginCApiRegisterWithRegistrar(
    FlutterDesktopPluginRegistrarRef registrar) {
  ff_tts_win::FfTtsWinPlugin::RegisterWithRegistrar(
      flutter::PluginRegistrarManager::GetInstance()
          ->GetRegistrar<flutter::PluginRegistrarWindows>(registrar));
}
