#ifndef _TTS_H_
#define _TTS_H_

#include <Windows.h>
#include <string>
#include "sapi.h"
#pragma comment(lib, "sapi.lib")

class TTS
{
public:
	TTS();
	~TTS();

	bool Init();
	void Dispose();
	bool Rate(long rate);
	bool Speak(std::string str);

private:
	ISpVoice* m_pSpVoice;

	void threadSpeak(std::wstring wstr);
	std::wstring StringToWString(std::string str);
};

#endif
